const sendDataToBucket = require('./sendDataToBucket');
const createBackup = require('./createBackup')

const main = async () => {
    const filename = await createBackup()
    await sendDataToBucket(filename)
}

main()