const { Storage } = require('@google-cloud/storage');
const bucketName = 'cron-backup';

const sendDataToBucket = async (file) => {
  try {

    const storage = new Storage({
      projectId: 'cron-backup',
      keyFilename: './gcs.json',
    });

    await storage
      .bucket(bucketName)
      .upload(`${__dirname}/${file}`, {
        gzip: true,
        metadata: {
          cacheControl: 'public, max-age=31536000',
        },
      });

    await storage.bucket(bucketName).file(`${__dirname}/${file}`)
    
  } catch (error) {
    console.error(error);
  }
};

module.exports = sendDataToBucket;
