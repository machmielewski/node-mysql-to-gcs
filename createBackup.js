const mysqldump = require('mysqldump');
const dumpFileName = `${new Date(Date.now()).toISOString()}.dump.sql`;

const createBackup = async () => {
    
    const connection = {
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'employees',
    }
    
    try {
    await mysqldump({
        connection, 
        dumpToFile: dumpFileName
    });

    return dumpFileName
    } catch(error) {
      console.error(error)  
    }
}

module.exports = createBackup